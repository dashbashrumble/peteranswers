//
//  ConfigurationViewController.swift
//  PeterAnswers
//
//  Created by Rahul Manghnani on 3/31/18.
//  Copyright © 2018 Rahul Manghnani. All rights reserved.
//

import UIKit
import Crashlytics

class ConfigurationViewController: UIViewController {

    @IBOutlet weak var answerTextView: UITextView!
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var updateLabel: UILabel!
    @IBOutlet weak var customAnswersButton: UIButton!
    @IBOutlet weak var defaultAnswersButton: UIButton!
    @IBOutlet weak var bothAnswersButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let passcodeSet = AppProvider.userDefaults.bool(forKey: Keys.passcodeSet)
        var vcs = navigationController?.viewControllers
        let count = vcs!.count
        if passcodeSet {
            vcs?.remove(at: count - 2)
        } else {
            vcs?.remove(at: count - 2)
            vcs?.remove(at: count - 2)
        }
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(QuestionViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
        answerTextView.delegate = self
        navigationController?.setViewControllers(vcs!, animated: true)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        configureActionButtons()
    }
    
    private func configureActionButtons() {
        let answers = AppProvider.userDefaults.object(forKey: Keys.userAnswers) as? [String] ?? [String]()
        if answers.count > 0 {
            customAnswersButton.isEnabled = true
            bothAnswersButton.isEnabled = true
            customAnswersButton.alpha = 1.0
            bothAnswersButton.alpha = 1.0
        } else {
            customAnswersButton.isEnabled = false
            bothAnswersButton.isEnabled = false
            customAnswersButton.alpha = 0.5
            bothAnswersButton.alpha = 0.5
        }
    }
    
    @IBAction func addAnswer(_ sender: Any) {
        answerTextView.resignFirstResponder()
        if answerTextView.text.isEmpty == false {
            var answers = AppProvider.userDefaults.object(forKey: Keys.userAnswers) as? [String] ?? [String]()
            let answer = answerTextView.text!
            answers.append(answer)
            Answers.logCustomEvent(withName: "New Answer Added",
                                   customAttributes: [
                                    "Answer String": answer
                ])
            AppProvider.userDefaults.set(answers, forKey: Keys.userAnswers)
            showUpdate(message: "Answer added successfully")
            configureActionButtons()
        } else {
            showError(message: "Please enter an answer")
        }
    }
    
    @IBAction func useSavedAnswers(_ sender: Any) {
        Answers.logCustomEvent(withName: "Answers Setting",
                               customAttributes: [
                                "Setting": AnswersSetting.custom.rawValue
            ])
        updateAnswersSetting(setting: .custom)
    }
    
    @IBAction func useDefaultAnswers(_ sender: Any) {
        Answers.logCustomEvent(withName: "Answers Setting",
                               customAttributes: [
                                "Setting": AnswersSetting.defaultAnswers.rawValue
            ])
        updateAnswersSetting(setting: .defaultAnswers)
    }
    
    @IBAction func useBothAnswers(_ sender: Any) {
        Answers.logCustomEvent(withName: "Answers Setting",
                               customAttributes: [
                                "Setting": AnswersSetting.defaultAnswers.rawValue
            ])
        updateAnswersSetting(setting: .both)
    }

    private func updateAnswersSetting(setting: AnswersSetting) {
        AppProvider.userDefaults.set(setting.rawValue, forKey: Keys.answersSetting)
        SwUtility.answersSetting = setting.rawValue
        showUpdate(message: "Updated!")
    }
    
    private func showError(message: String) {
        errorLabel.text = message
        UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseIn, animations: {
            self.errorLabel.alpha = 1.0
        }) { (finished) in
            DispatchQueue.main.asyncAfter(deadline: .now() + 5.0, execute: {
                UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseOut, animations: {
                    self.errorLabel.alpha = 0.0
                }, completion: nil)
            })
        }
    }
    
    private func showUpdate(message: String) {
        updateLabel.text = message
        UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseIn, animations: {
            self.updateLabel.alpha = 1.0
        }) { (finished) in
            DispatchQueue.main.asyncAfter(deadline: .now() + 5.0, execute: {
                UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseOut, animations: {
                    self.updateLabel.alpha = 0.0
                }, completion: nil)
            })
        }
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}

extension ConfigurationViewController: UITextViewDelegate {
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
}
