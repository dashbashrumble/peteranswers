//
//  ViewController.swift
//  PeterAnswers
//
//  Created by Rahul Manghnani on 3/21/18.
//  Copyright © 2018 Rahul Manghnani. All rights reserved.
//

import UIKit
import GoogleMobileAds
import Crashlytics

class ViewController: UIViewController, GADBannerViewDelegate {

    @IBOutlet weak var newUserButton: UIButton!
    @IBOutlet weak var iKnowLabel: UILabel!
    @IBOutlet weak var configureButton: UIButton!
    @IBOutlet weak var quickStartButton: UIButton!
    @IBOutlet weak var peterAnswersTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var bannerView: GADBannerView!
    
    var navBarHeight: CGFloat = 0
    var isInitialLayoutDone = false
    var localTimeZoneAbbreviation: String { return TimeZone.current.abbreviation() ?? "" }
    var localTimeZoneName: String { return TimeZone.current.identifier }
    var systemVersion: String { return UIDevice.current.systemVersion }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        bannerView.adUnitID = "ca-app-pub-9043117893678177/5156449684"  
        bannerView.rootViewController = self
        bannerView.load(GADRequest())
        bannerView.delegate = self
        bannerView.alpha = 0.0
        if AppProvider.userDefaults.object(forKey: Keys.answersSetting) == nil {
            AppProvider.userDefaults.set(AnswersSetting.defaultAnswers.rawValue, forKey: Keys.answersSetting)
            SwUtility.answersSetting = AnswersSetting.defaultAnswers.rawValue
        } else if let setting = AppProvider.userDefaults.object(forKey: Keys.answersSetting) as? String {
            SwUtility.answersSetting = setting
        }

        if AppProvider.userDefaults.object(forKey: Keys.launchEvents) == nil {
            AppProvider.userDefaults.set("Events sent", forKey: Keys.launchEvents)
            Answers.logCustomEvent(withName: "Launch",
                                   customAttributes: [
                                    "Time": getTodayString(),
                                    "Time Zone Abbreviation": localTimeZoneAbbreviation,
                                    "Time Zone Name": localTimeZoneName,
                                    "OS Version": systemVersion
                ])
            if let countryCode = NSLocale.current.regionCode {
                Answers.logCustomEvent(withName: "Country",
                                       customAttributes: [
                                        "Country Code": countryCode
                    ])
            }
        } 
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        peterAnswersTopConstraint.constant = 50
        UIView.animate(withDuration: 0.4, delay: 0.5, options: UIViewAnimationOptions.curveLinear, animations: {
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if !isInitialLayoutDone, let navigationBar = self.navigationController?.navigationBar {
            isInitialLayoutDone = true
            navBarHeight = navigationBar.frame.height 
            peterAnswersTopConstraint.constant = 200 - navBarHeight
        }
    }
    
    @IBAction func teachMe(_ sender: Any) {
        Answers.logCustomEvent(withName: "Home Page Button",
                               customAttributes: [
                                "Action": "Teacher"
            ])
        self.performSegue(withIdentifier: "passcode", sender: true)
    }
    
    @IBAction func configure(_ sender: Any) {
        Answers.logCustomEvent(withName: "Home Page Button",
                               customAttributes: [
                                "Action": "Configuration"
            ])
    }
    
    @IBAction func quickStart(_ sender: Any) {
        Answers.logCustomEvent(withName: "Home Page Button",
                               customAttributes: [
                                "Action": "Quick Start"
            ])
    }
    
    func getTodayString() -> String{
        let date = Date()
        let calender = Calendar.current
        let components = calender.dateComponents([.year,.month,.day,.hour,.minute,.second], from: date)
        
        let year = components.year
        let month = components.month
        let day = components.day
        let hour = components.hour
        let minute = components.minute
        let second = components.second
        
        let today_string = String(year!) + "-" + String(month!) + "-" + String(day!) + " " + String(hour!)  + ":" + String(minute!) + ":" +  String(second!)
        
        return today_string
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let identifier = segue.identifier else { return }
        switch identifier {
        case "passcode":
            let viewController = segue.destination as! PasscodeViewController
            viewController.navigateToTeacher = (sender as? Bool) ?? false
        default:
            break
        }
    }
    
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        bannerView.alpha = 0
        
        if ( UIDevice.current.model.range(of: "iPad") != nil){
            // Do nothing
        } else {
            UIView.animate(withDuration: 1, animations: {
                bannerView.alpha = 1
            })
        }
    }
}

extension UINavigationController {
    func setTransparentNavigationBar(_ enable: Bool) {
        if enable {
            makeNavigationBarTransparent(UIImage())
        } else {
            makeNavigationBarVisible()
        }
    }
    
    func addOverlay(_ imageName: String) {
        guard let image = UIImage(named: imageName) else { return }
        makeNavigationBarTransparent(image)
    }
    
    func removeOverlay() {
        makeNavigationBarVisible()
    }
    
    func makeNavigationBarTransparent(_ image: UIImage) {
        navigationBar.setBackgroundImage(image, for: .default)
        navigationBar.shadowImage = UIImage()
        navigationBar.isTranslucent = true
        view.backgroundColor = UIColor.clear
        navigationBar.backgroundColor = UIColor.clear
    }
    
    func makeNavigationBarVisible() {
        navigationBar.setBackgroundImage(nil, for: .default)
        navigationBar.shadowImage = nil
        navigationBar.isTranslucent = false
        view.backgroundColor = UIColor.white
        navigationBar.backgroundColor = UIColor.white
    }
}
