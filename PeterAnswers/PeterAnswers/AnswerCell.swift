//
//  AnswerCell.swift
//  PeterAnswers
//
//  Created by Rahul Manghnani on 3/31/18.
//  Copyright © 2018 Rahul Manghnani. All rights reserved.
//

import UIKit

class AnswerCell: UITableViewCell {

    @IBOutlet weak var answerView: UIView!
    @IBOutlet weak var answerLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        answerView.layer.cornerRadius = 5
        answerView.layer.masksToBounds = true
        self.backgroundColor = UIColor.clear
        self.selectionStyle = .none
    }

    func initializeCell(answer: String) {
        answerLabel.text = answer
    }
    
}
