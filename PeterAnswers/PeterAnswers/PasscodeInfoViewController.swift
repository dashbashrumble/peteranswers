//
//  PasscodeInfoViewController.swift
//  PeterAnswers
//
//  Created by Rahul Manghnani on 3/31/18.
//  Copyright © 2018 Rahul Manghnani. All rights reserved.
//

import UIKit

class PasscodeInfoViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        var vcs = navigationController?.viewControllers
        let count = vcs!.count
        vcs?.remove(at: count - 2)
        navigationController?.setViewControllers(vcs!, animated: true)
    }
}
