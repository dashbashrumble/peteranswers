//
//  DefaultAnswers.swift
//  PeterAnswers
//
//  Created by Rahul Manghnani on 3/31/18.
//  Copyright © 2018 Rahul Manghnani. All rights reserved.
//

import Foundation

struct DefaultAnswers {
    static let answers = [
        "Trust and thou shall know.",
        "If you continue being rude to me, I will never answer.",
        "Maybe I'll try to help you.",
        "Ask again, I'm thinking.",
        "I am too weak to answer.",
        "We'll leave this for later.",
        "I'll answer that when I want to.",
        "I am not capable of answering this.",
        "I can't help you.",
        "Once you have gained my trust, you'll have my answer.",
        "That's not a good question.",
        "That is not important at this time.",
        "Ask me this again at the stroke of midnight.",
        "Let us leave this question for another time.",
        "I think you are testing me, try again.",
        "You are not allowed to ask.",
        "I'm not answering that question.",
        "I will tell you when you are ready.",
        "I would feel better if another person asked.",
        "I prefer not to comment",
        "I don't feel like answering that.",
        "Trust me",
        "I'm sorry, without cooperation I can't work.",
        "Time answers everything."
    ]
    
    static func getDefaultAnswer() -> String {
        let randomIndex = Int(arc4random_uniform(UInt32(answers.count)))
        return answers[randomIndex]
    }
}
