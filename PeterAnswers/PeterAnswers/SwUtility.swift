//
//  SwUtility.swift
//  PeterAnswers
//
//  Created by Rahul Manghnani on 3/21/18.
//  Copyright © 2018 Rahul Manghnani. All rights reserved.
//

import Foundation
import UIKit

struct Keys {
    static let firstLaunch = "isFirstLaunch"
    static let passcodeSet = "passcodeSet"
    static let userAnswers = "userAnswers"
    static let passcode = "passcode"
    static let answersSetting = "answersSetting"
    static let launchEvents = "launchEvents"
}

struct GlobalConstants {
    static let iOSVersion = SwUtility.getOSVersion()
    static let appVersion = SwUtility.getAppVersion()
    static let deviceIdentifier = SwUtility.getUniqueIndentifier()
    static let versionCode = SwUtility.versionCode()
    static let screenWidth = SwUtility.getScreenWidth()
    static let screenHeight = SwUtility.getScreenHeight()
}

final class SwUtility {

    static var answersSetting = AnswersSetting.defaultAnswers.rawValue
    
    class func getScreenWidth() -> CGFloat {
        return UIScreen.main.bounds.size.width
    }
    
    class func getScreenHeight() -> CGFloat {
        return UIScreen.main.bounds.size.height
    }
    
    class func getUniqueIndentifier() -> String {
        guard let identifider = UIDevice.current.identifierForVendor?.uuidString else {
            return ""
        }
        return identifider
    }
    
    class func getAppVersion() -> String {
        var myDict: NSDictionary?
        if let path = Bundle.main.path(forResource: "Info", ofType: "plist") {
            myDict = NSDictionary(contentsOfFile: path)
        }
        if let dict = myDict {
            return dict.value(forKey: "CFBundleShortVersionString") as! String
        } else {
            return "2.0.8"
        }
    }
    
    class func versionCode() -> String {
        var versionArray = GlobalConstants.appVersion.components(separatedBy: ".")
        var versionInt = 0
        var divisor = 100
        for i in 0 ..< versionArray.count {
            if let string = versionArray[i] as String? {
                if let toInt = Int(string) {
                    versionInt += (divisor * toInt)
                    divisor /= 10
                }
            }
        }
        return "\(versionInt)"
    }
    
    class func getOSVersion() -> String {
        return UIDevice.current.systemVersion
    }
    
    class func systemVersionLessThan(_ version: String) -> Bool {
        return UIDevice.current.systemVersion.compare(version,
                                                      options: NSString.CompareOptions.numeric) == ComparisonResult.orderedAscending
    }
    
    class func systemVersionGreaterThanOrEqualTo(_ version: String) -> Bool {
        return UIDevice.current.systemVersion.compare(version,
                                                      options: NSString.CompareOptions.numeric) != ComparisonResult.orderedAscending
    }
    
    class func is4sDevice() -> Bool {
        if UIScreen.main.bounds.size.height <= 480 {
            return true
        } else {
            return false
        }
    }
    
    // 5, 5s and below
    class func isSmallDevice(_ view: UIView) -> Bool {
        if view.frame.size.width <= 320 {
            return true
        } else {
            return false
        }
    }
    
    class func isSmallDevice() -> Bool {
        if UIScreen.main.bounds.size.width <= 320 {
            return true
        } else {
            return false
        }
    }
    
    class func clearUserData() {
        AppProvider.userDefaults.set(false, forKey: Keys.passcodeSet)
        AppProvider.userDefaults.remove(forKey: Keys.passcode)
        AppProvider.userDefaults.remove(forKey: Keys.userAnswers)
        AppProvider.userDefaults.set(AnswersSetting.defaultAnswers.rawValue, forKey: Keys.answersSetting)
    }
    
    class func removeUserAnswers() {
        AppProvider.userDefaults.remove(forKey: Keys.userAnswers)
    }
}

enum AnswersSetting: String {
    case custom = "custom"
    case defaultAnswers = "defaultAnswers"
    case both = "both"
}
