//
//  AnswersViewController.swift
//  PeterAnswers
//
//  Created by Rahul Manghnani on 3/31/18.
//  Copyright © 2018 Rahul Manghnani. All rights reserved.
//

import UIKit
import GoogleMobileAds

class AnswersViewController: UIViewController, GADBannerViewDelegate {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var bannerView: GADBannerView!
    
    var answers = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        answers = AppProvider.userDefaults.object(forKey:Keys.userAnswers) as? [String] ?? [String]()
        if answers.count == 0 {
          titleLabel.text = "You have no saved answers. Go back and add some!"
            deleteButton.isHidden = true
            deleteButton.isUserInteractionEnabled = false
            deleteButton.isEnabled = false
        }
        tableView.register(UINib(nibName: "AnswerCell", bundle: Bundle.main), forCellReuseIdentifier: "AnswerCell")
        tableView.dataSource = self
        tableView.delegate = self
        tableView.estimatedRowHeight = 50.0
        tableView.backgroundColor = UIColor.clear
        
        bannerView.adUnitID = "ca-app-pub-9043117893678177/7443701676"
        bannerView.rootViewController = self
        bannerView.load(GADRequest())
        bannerView.delegate = self
        bannerView.alpha = 0.0
    }
    
    @IBAction func deleteAllAnswers(_ sender: Any) {
        let alert = UIAlertController(title: "Are you sure?", message: "All your saved answers will be deleted", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (action) in
            SwUtility.removeUserAnswers()
            self.navigationController?.popViewController(animated: true)
        }))
        alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        bannerView.alpha = 0
        if ( UIDevice.current.model.range(of: "iPad") != nil){
            // Do nothing
        } else {
            UIView.animate(withDuration: 1, animations: {
                bannerView.alpha = 1
            })
        }
    }
}

extension AnswersViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return answers.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
}

extension AnswersViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AnswerCell", for: indexPath) as! AnswerCell
        cell.initializeCell(answer: answers[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
}
