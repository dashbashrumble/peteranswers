//
//  LastTeacherViewController.swift
//  PeterAnswers
//
//  Created by Rahul Manghnani on 4/2/18.
//  Copyright © 2018 Rahul Manghnani. All rights reserved.
//

import UIKit
import GoogleMobileAds

class LastTeacherViewController: UIViewController, GADInterstitialDelegate {

    var interstitial: GADInterstitial!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        interstitial = createAndLoadInterstitial()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        DispatchQueue.main.asyncAfter(deadline: .now() + 5.0, execute: {
            self.showInterstitial()
        })
    }
    
    private func showInterstitial() {
        if interstitial.isReady {
            interstitial.present(fromRootViewController: self)
        }
    }
    
    func createAndLoadInterstitial() -> GADInterstitial {
        let interstitial = GADInterstitial(adUnitID: "ca-app-pub-9043117893678177/8481806397")
        interstitial.delegate = self
        interstitial.load(GADRequest())
        return interstitial
    }
    
    func interstitialDidDismissScreen(_ ad: GADInterstitial) {
        interstitial = createAndLoadInterstitial()
    }
}
