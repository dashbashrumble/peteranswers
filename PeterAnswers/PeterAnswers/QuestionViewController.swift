//
//  QuestionViewController.swift
//  PeterAnswers
//
//  Created by Rahul Manghnani on 3/21/18.
//  Copyright © 2018 Rahul Manghnani. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import GoogleMobileAds
import Crashlytics

class QuestionViewController: UIViewController, NVActivityIndicatorViewable, GADInterstitialDelegate, GADBannerViewDelegate {
    
    @IBOutlet weak var answerLabel: UILabel!
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var petitionLabel: UILabel!
    @IBOutlet weak var petitionTextView: UITextView!
    @IBOutlet weak var questionLabel: UILabel!
    @IBOutlet weak var questionTextView: UITextView!
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var bannerView: GADBannerView!
    
    var recordedAnswerStack = Stack<String>()
    var answerInProgress = false
    var answerComplete = false
    var lastRecordedTextLength = 0
    var backSpacePressed = false
    var peterPleaseAnswerString = ["P", "e", "t", "e", "r", " ", "p", "l", "e", "a", "s", "e", " ", "a", "n", "s", "w", "e", "r", " ", "t", "h", "e", " ", "f", "o", "l", "l", "o", "w", "i", "n", "g", " ", "q", "u", "e", "s", "t", "i", "o", "n"]
    var adCount = 0
    var interstitial: GADInterstitial!
    
    var isSubmitButtonEnabled: Bool = false {
        didSet {
            submitButton.isEnabled = isSubmitButtonEnabled
            if isSubmitButtonEnabled == true {
                submitButton.alpha = 1.0
            } else {
                submitButton.alpha = 0.5
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        errorLabel.alpha = 0.0
        petitionTextView.delegate = self
        questionTextView.delegate = self
        petitionTextView.tintColor = UIColor.paYellow
        questionTextView.tintColor = UIColor.paYellow
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(QuestionViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
        
        interstitial = createAndLoadInterstitial()
        isSubmitButtonEnabled = false
        
        bannerView.adUnitID = "ca-app-pub-9043117893678177/5809420294"
        bannerView.rootViewController = self
        bannerView.load(GADRequest())
        bannerView.delegate = self
        bannerView.alpha = 0.0
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    @IBAction func submit(_ sender: Any) {
        submitButtonAction()
    }
    
    private func submitButtonAction() {
        if submitButton.titleLabel?.text == "New Question" {
            questionTextView.text = ""
            resetPetition()
            self.questionTextView.isEditable = true
            self.petitionTextView.isEditable = true
            answerLabel.text = ""
            isSubmitButtonEnabled = false
            submitButton.setTitle("Submit", for: .normal)
            submitButton.setTitle("Submit", for: .selected)
            return
        }
        if petitionTextView.text.isEmpty == true {
            showError(message: "Please enter a petition")
        } else if questionTextView.text.isEmpty == true {
            showError(message: "Please enter a question")
        } else {
            self.petitionTextView.resignFirstResponder()
            self.questionTextView.resignFirstResponder()
            startAnimating(message: "Calculating the result", type: NVActivityIndicatorType.ballScaleRippleMultiple, color: UIColor.paYellow, textColor: UIColor.paYellow)
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.4, execute: {
                self.stopAnimating()
                let answer = self.getCompleteAnswer()
                self.answerLabel.text = answer
                Answers.logCustomEvent(withName: "Answer",
                                       customAttributes: [
                                        "Answer String": answer
                    ])
                Answers.logCustomEvent(withName: "Question",
                                       customAttributes: [
                                        "Question String": self.questionTextView.text
                    ])
                self.adCount += 1
                self.questionTextView.isEditable = false
                self.petitionTextView.isEditable = false
                self.submitButton.setTitle("New Question", for: .normal)
                self.submitButton.setTitle("New Question", for: .selected)
                self.isSubmitButtonEnabled = true
            })
            if adCount == 2 {
                adCount = 0
                DispatchQueue.main.asyncAfter(deadline: .now() + 4.0, execute: {
                    self.showInterstitial()
                })
            }
        }
    }
    
    private func showInterstitial() {
        if interstitial.isReady {
            interstitial.present(fromRootViewController: self)
        }
    }
    
    func createAndLoadInterstitial() -> GADInterstitial {
        let interstitial = GADInterstitial(adUnitID: "ca-app-pub-9043117893678177/8960318549")
        interstitial.delegate = self
        interstitial.load(GADRequest())
        return interstitial
    }
    
    func interstitialDidDismissScreen(_ ad: GADInterstitial) {
        interstitial = createAndLoadInterstitial()
    }
    
    private func showError(message: String) {
        errorLabel.text = message
        UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseIn, animations: {
            self.errorLabel.alpha = 1.0
        }) { (finished) in
            DispatchQueue.main.asyncAfter(deadline: .now() + 5.0, execute: {
                UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseOut, animations: {
                    self.errorLabel.alpha = 0.0
                }, completion: nil)
            })
        }
    }
    @IBAction func clearPetition(_ sender: Any) {
        resetPetition()
    }
    
    private func resetPetition() {
        recordedAnswerStack.array.removeAll()
        petitionTextView.text = ""
        answerInProgress = false
        answerComplete = false
    }
    
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        bannerView.alpha = 0
        
        if ( UIDevice.current.model.range(of: "iPad") != nil){
            // Do nothing
        } else {
            UIView.animate(withDuration: 1, animations: {
                bannerView.alpha = 1
            })
        }
    }
}

extension QuestionViewController {
    private func getCompleteAnswer() -> String {
        var completeAnswer = ""
        if answerComplete == true {
            for element in recordedAnswerStack {
                completeAnswer += element
            }
        }
        if completeAnswer.isEmpty == true {
            if let setting = AppProvider.userDefaults.object(forKey: Keys.answersSetting) as? String {
                switch setting {
                case AnswersSetting.custom.rawValue:
                    var answers = AppProvider.userDefaults.object(forKey: Keys.userAnswers) as? [String] ?? [String]()
                    let randomIndex = Int(arc4random_uniform(UInt32(answers.count)))
                    return answers[randomIndex]
                case AnswersSetting.defaultAnswers.rawValue:
                    return DefaultAnswers.getDefaultAnswer()
                case AnswersSetting.both.rawValue:
                    let randomNumber = randomBetween(min: 1, max: 3)
                    var answers = AppProvider.userDefaults.object(forKey: Keys.userAnswers) as? [String] ?? [String]()
                    if randomNumber == 1 && answers.count > 0 {
                        let randomIndex = Int(arc4random_uniform(UInt32(answers.count)))
                        return answers[randomIndex]
                    } else {
                        return DefaultAnswers.getDefaultAnswer()
                    }
                default:
                    return ""
                }
            }
        }
        recordedAnswerStack.array.removeAll()
        return String(completeAnswer.reversed())
    }
    
    private func replaceWithDefaultPetition(textView: UITextView) {
        if answerComplete == false {
            recordedAnswerStack.push(String(describing: textView.text.last!))
        }
        textView.text = String(textView.text.dropLast())
        if recordedAnswerStack.count <= peterPleaseAnswerString.count {
            textView.text = textView.text + peterPleaseAnswerString[recordedAnswerStack.count - 1]
        } else {
            textView.text = textView.text + " "
        }
    }
}

extension QuestionViewController: UITextViewDelegate {
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if textView == petitionTextView {
            if text.isEmpty == true {
                backSpacePressed = true
            } else {
                backSpacePressed = false
            }
        }
        if text == "\n" {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    
    func textViewDidChange(_ textView: UITextView) {
        
        if petitionTextView.text.isEmpty == false && questionTextView.text.isEmpty == false {
            isSubmitButtonEnabled = true
            submitButton.setTitle("Submit", for: .normal)
            submitButton.setTitle("Submit", for: .selected)
        } else {
            isSubmitButtonEnabled = false
            submitButton.setTitle("Submit", for: .normal)
            submitButton.setTitle("Submit", for: .selected)
        }
        
        if textView == questionTextView {
            if textView.text.last != nil && String(describing: textView.text.last!) == "?" {
                submitButtonAction()
            }
            return
        }
        
        if backSpacePressed == true {
            if answerInProgress == true {
                recordedAnswerStack.array.removeAll()
                answerInProgress = false
                answerComplete = false
            }
        }
        
        
        if textView.text.last != nil && String(describing: textView.text.last!) == "." && answerInProgress == false && answerComplete == false && backSpacePressed == false {
            answerInProgress = true
            //answer just started
            //replace with P
            // don't record
            textView.text = String(textView.text.dropLast())
            textView.text = textView.text + peterPleaseAnswerString[0]
        } else if textView.text.last != nil && String(describing: textView.text.last!) == "." && answerInProgress == true && answerComplete == false && backSpacePressed == false   {
            answerComplete = true
            answerInProgress = false // Just updating for context
            //answer completed
            //replace
            // don't record
            textView.text = String(textView.text.dropLast())
            if (recordedAnswerStack.count + 1) < peterPleaseAnswerString.count {
                textView.text = textView.text + peterPleaseAnswerString[recordedAnswerStack.count + 1]
            } else {
                textView.text = textView.text + " "
            }
        } else if textView.text.last != nil && String(describing: textView.text.last!) != "." && answerInProgress == true && answerComplete == false && backSpacePressed == false   {
            // answer in progress
            // push to stack
            // replace
            recordedAnswerStack.push(String(describing: textView.text.last!))
            textView.text = String(textView.text.dropLast())
            if recordedAnswerStack.count < peterPleaseAnswerString.count {
                textView.text = textView.text + peterPleaseAnswerString[recordedAnswerStack.count]
            } else {
                textView.text = textView.text + " "
            }
        } else {
            // Do nothing
        }
    }
}

import GameKit

func randomBetween(min: Int, max: Int) -> Int {
    return GKRandomSource.sharedRandom().nextInt(upperBound: max - min) + min
}

enum CharacterCountReplacement: String {
    case One = "P"
    case Two = "Pe"
    case Three = "Pet"
    case Four = "Pete"
    case Five = "Peter"
    case Six = "Peter "
    case Seven = "Peter p"
    case Eight = "Peter pl"
    case Nine = "Peter ple"
    case Ten = "Peter plea"
    case Eleven = "Peter pleas"
    case Twelve = "Peter please"
    case Thirteen = "Peter please "
    case Fourteen = "Peter please a"
    case Fifteen = "Peter please an"
    case Sixteen = "Peter please ans"
    case Seventeen = "Peter please answ"
    case Eighteen = "Peter please answe"
    case Nineteen = "Peter please answer"
    case Twenty = "Peter please answer "
    case Twenty1 = "Peter please answer t"
    case Twenty2 = "Peter please answer th"
    case Twenty3 = "Peter please answer the"
    case Twenty4 = "Peter please answer the "
    case Twenty5 = "Peter please answer the f"
    case Twenty6 = "Peter please answer the fo"
    case Twenty7 = "Peter please answer the fol"
    case Twenty8 = "Peter please answer the foll"
    case Twenty9 = "Peter please answer the follo"
    case Thirty = "Peter please answer the follow"
    case Thirty1 = "Peter please answer the followi"
    case Thirty2 = "Peter please answer the followin"
    case Thirty3 = "Peter please answer the following"
    case Thirty4 = "Peter please answer the following "
    case Thirty5 = "Peter please answer the following q"
    case Thirty6 = "Peter please answer the following qu"
    case Thirty7 = "Peter please answer the following que"
    case Thirty8 = "Peter please answer the following ques"
    case Thirty9 = "Peter please answer the following quest"
    case Forty = "Peter please answer the following questi"
    case Forty1 = "Peter please answer the following questio"
    case Forty2 = "Peter please answer the following question"
}

public struct Stack<T> {
    fileprivate var array = [T]()
    
    public var isEmpty: Bool {
        return array.isEmpty
    }
    
    public var count: Int {
        return array.count
    }
    
    public mutating func push(_ element: T) {
        array.append(element)
    }
    
    public mutating func pop() -> T? {
        return array.popLast()
    }
    
    public var top: T? {
        return array.last
    }
}

extension Stack: Sequence {
    public func makeIterator() -> AnyIterator<T> {
        var curr = self
        return AnyIterator {
            return curr.pop()
        }
    }
}
