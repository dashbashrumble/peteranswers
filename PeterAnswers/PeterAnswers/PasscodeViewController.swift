//
//  PasscodeViewController.swift
//  PeterAnswers
//
//  Created by Rahul Manghnani on 3/27/18.
//  Copyright © 2018 Rahul Manghnani. All rights reserved.
//

import UIKit

class PasscodeViewController: UIViewController {

    @IBOutlet weak var passcodeLabel: UILabel!
    @IBOutlet weak var textfield1: UITextField!
    @IBOutlet weak var textfield2: UITextField!
    @IBOutlet weak var textfield3: UITextField!
    @IBOutlet weak var textfield4: UITextField!
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var forgotPasswordButton: UIButton!
    @IBOutlet weak var clearDataButton: UIButton!
    
    var newPasscode = [String](repeating: "", count: 4)
    var confirmPasscode = [String](repeating: "", count: 4)
    var confirmPasswordIP = false
    var passcodeSaved = false
    var navigateToTeacher = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        textfield1.tintColor = UIColor.paYellow
        textfield2.tintColor = UIColor.paYellow
        textfield3.tintColor = UIColor.paYellow
        textfield4.tintColor = UIColor.paYellow
        
        textfield1.isSecureTextEntry = true
        textfield2.isSecureTextEntry = true
        textfield3.isSecureTextEntry = true
        textfield4.isSecureTextEntry = true
        
        textfield1.delegate = self
        textfield1.tag = 1
        textfield2.delegate = self
        textfield2.tag = 2
        textfield3.delegate = self
        textfield3.tag = 3
        textfield4.delegate = self
        textfield4.tag = 4
        
        errorLabel.alpha = 0.0
        clearDataButton.alpha = 0.0
        
        passcodeSaved = AppProvider.userDefaults.bool(forKey: Keys.passcodeSet)
        if passcodeSaved == true {
            passcodeLabel.text = "Enter your passcode"
            forgotPasswordButton.isHidden = false
            forgotPasswordButton.isUserInteractionEnabled = true
        } else {
            passcodeLabel.text = "Set up a new passcode"
            forgotPasswordButton.isHidden = true
            forgotPasswordButton.isUserInteractionEnabled = false
        }
        clearDataButton.titleLabel?.lineBreakMode = .byWordWrapping
        clearDataButton.titleLabel?.numberOfLines = 0
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        textfield1.becomeFirstResponder()
    }
    
    private func nextResponder(textField: UITextField) {
        switch textField.tag {
        case 1:
            textfield2.becomeFirstResponder()
        case 2:
            textfield3.becomeFirstResponder()
        case 3:
            textfield4.becomeFirstResponder()
        default:
            break
        }
        if textfield1.text?.isEmpty == false &&
            textfield2.text?.isEmpty == false &&
            textfield3.text?.isEmpty == false &&
            textfield4.text?.isEmpty == false  {
            if confirmPasswordIP {
                verifyPassword()
            } else {
                if passcodeSaved {
                    verifySavedPassword()
                } else {
                    confirmPassword()
                    confirmPasswordIP = true
                }
            }
        }
    }
    
    private func previousResponder(textField: UITextField) {
        switch textField.tag {
        case 1:
            break
        case 2:
            textfield1.becomeFirstResponder()
        case 3:
            textfield2.becomeFirstResponder()
        case 4:
            textfield3.becomeFirstResponder()
        default:
            break
        }
    }
    
    private func updatePasscode(textField: UITextField, digit: String) {
        if confirmPasswordIP {
            switch textField.tag {
            case 1:
                confirmPasscode[0] = digit //insert(digit, at: 0)
            case 2:
                confirmPasscode[1] = digit //insert(digit, at: 1)
            case 3:
                confirmPasscode[2] = digit //insert(digit, at: 2)
            case 4:
                confirmPasscode[3] = digit //insert(digit, at: 3)
            default:
                break
            }
        } else {
            switch textField.tag {
            case 1:
                newPasscode[0] = digit //insert(digit, at: 0)
            case 2:
                newPasscode[1] = digit //insert(digit, at: 1)
            case 3:
                newPasscode[2] = digit //insert(digit, at: 2)
            case 4:
                newPasscode[3] = digit //insert(digit, at: 3)
            default:
                break
            }
        }
    }
    
    private func verifySavedPassword() {
        let entered = getPasscode()
        let saved = AppProvider.userDefaults.object(forKey: Keys.passcode) as? String
        if entered == saved {
            if navigateToTeacher {
                self.performSegue(withIdentifier: "teacher", sender: nil)
            } else {
                self.performSegue(withIdentifier: "savedPasscodeShowConfig", sender: nil)
            }
        } else {
            showError(message: "Wrong passcode")
        }
    }
    
    private func confirmPassword() {
        var canConfirmPasscode = true
        for digit in newPasscode {
            if digit.isEmpty == true {
                canConfirmPasscode = false
            }
        }
        if canConfirmPasscode == true {
            passcodeLabel.text = "Confirm passcode"
            textfield1.isSecureTextEntry = false
            textfield2.isSecureTextEntry = false
            textfield3.isSecureTextEntry = false
            textfield4.isSecureTextEntry = false
            textfield1.text = ""
            textfield2.text = ""
            textfield3.text = ""
            textfield4.text = ""
            textfield1.becomeFirstResponder()
        } else {
            showError(message: "Please enter all 4 digits for a valid passcode")
        }
    }
    
    private func verifyPassword() {
        var passwordVerficationPassed = true
        for index in 0...3 {
            if newPasscode[index] != confirmPasscode[index] {
                passwordVerficationPassed = false
            }
        }
        if passwordVerficationPassed {
            AppProvider.userDefaults.set(getPasscode(), forKey: Keys.passcode)
            AppProvider.userDefaults.set(true, forKey: Keys.passcodeSet)
            if navigateToTeacher {
                self.performSegue(withIdentifier: "teacher", sender: nil)
            } else {
                self.performSegue(withIdentifier: "showConfigurationScreen", sender: nil)
            }
        } else {
            showError(message: "Passcode verification failed. Try again.")
        }
    }
    
    private func getPasscode() -> String {
        var passCode = ""
        for digit in newPasscode {
            passCode += digit
        }
        return passCode
    }
    
    private func showError(message: String) {
        errorLabel.text = message
        UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseIn, animations: {
            self.errorLabel.alpha = 1.0
        }) { (finished) in
            DispatchQueue.main.asyncAfter(deadline: .now() + 5.0, execute: {
                UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseOut, animations: {
                    self.errorLabel.alpha = 0.0
                }, completion: nil)
            })
        }
    }
    
    private func showClearData() {
        UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseIn, animations: {
            self.clearDataButton.alpha = 1.0
        }) { (finished) in
            DispatchQueue.main.asyncAfter(deadline: .now() + 5.0, execute: {
                UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseOut, animations: {
                    self.clearDataButton.alpha = 0.0
                }, completion: nil)
            })
        }
    }
    
    @IBAction func forgotPassword(_ sender: Any) {
        showClearData()
    }
    
    @IBAction func clearData(_ sender: Any) {
        SwUtility.clearUserData()
        self.navigationController?.popToRootViewController(animated: true)
    }
    
}

extension PasscodeViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        textField.text = string
        updatePasscode(textField: textField, digit: string)
        if string.isEmpty == true {
            previousResponder(textField: textField)
        } else {
            nextResponder(textField: textField)
        }
        return false
    }
}
